import time
import thumby
import math
import random


class State:
    WELCOME = 1
    MENU = 2
    EXIT = 3
    TUTORIAL = 4
    MATCH = 5
    ABOUT = 6
    END = 7

state = State.MATCH

MAX_X_VEL = 35
MAX_Y_VEL = 80
JUMP_VELOCITY = 90
GRAVITY = 1.8
DECELERATION = 1.5
ACCELERATION = 1.15

MAX_CAM_VELOCITY = 50
CAM_ACCELERATION = 1.7
CAM_DECELERATION = 2.5

TILE_LENGTH = 16
TILE_HEIGHT = 22

# TODO switch back to 100
ANIMATION_MS_DELAY = 100


def run_level(init_x_pos, init_y_pos, tile_map):
    # BITMAP: width: 3, height: 3
    cross = bytearray([5,2,5])
    # BITMAP: width: 12, height: 10
    cat_stand1 = bytearray([241,239,15,15,15,143,143,143,0,1,244,241,
               3,3,0,0,3,3,3,3,0,0,3,3])
    cat_stand2 = bytearray([253,227,15,15,15,143,143,143,0,1,244,241,
               3,3,0,0,3,3,3,3,0,0,3,3])
    cat_stand3 = bytearray([255,224,15,15,15,143,143,143,0,1,244,241,
               3,3,0,0,3,3,3,3,0,0,3,3])
    cat_stand4 = bytearray([243,236,15,15,15,143,143,143,0,1,240,241,
           3,3,0,0,3,3,3,3,0,0,3,3])
    cat_walk1 = bytearray([249,231,15,15,15,143,143,143,0,1,116,241,
            3,3,0,3,2,3,3,3,3,0,2,3])
    cat_walk2 = bytearray([253,99,15,15,15,143,143,143,0,0,245,241,
            2,2,3,1,2,3,3,3,0,3,0,3])
    cat_walk3 = bytearray([199,219,31,31,31,31,31,31,1,3,233,227,
            3,2,2,0,3,3,2,2,3,0,3,3])
    cat_walk4 = bytearray([227,223,31,31,31,31,31,31,1,3,233,227,
            3,3,0,2,3,3,3,3,2,0,3,3])
    cat_jump1 = bytearray([223,239,103,7,7,135,135,0,1,244,241,255,
            3,3,0,3,0,3,2,0,2,3,3,3])
    cat_jump2 = bytearray([191,207,111,143,15,199,195,0,0,250,248,255,
                3,3,0,0,3,3,3,2,3,3,3,3])
    cat_jump3 = bytearray([223,223,207,15,15,199,195,0,0,248,248,255,
                3,3,0,1,2,3,2,3,3,3,3,3])
    cat_jump4 = bytearray([159,239,103,7,131,193,193,129,64,177,116,241,
                3,3,2,2,3,3,3,3,3,3,3,3])
    cat_jump5 = bytearray([207,239,15,15,135,199,199,143,0,96,229,241,
            3,3,0,3,3,3,3,3,2,0,3,3])
    cat_standing = [cat_stand1, cat_stand1, cat_stand1, cat_stand2, cat_stand3, cat_stand4]
    cat_walking = [cat_stand1, cat_walk1, cat_walk2, cat_walk3]
    cat_jumping = [cat_jump1, cat_jump2, cat_jump3, cat_jump4, cat_jump5]
    # tile_map = [[1,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1],
    #             [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             [0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             [0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             [0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             [0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             [0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             [0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             [0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             [0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             [0,1,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             [1,1,1,1,1,1,1,0,0,0,1,1,0,0,0,0,1,1,1,1,0,0,0,0,1,1]]
    frame_index = 0
    last_anim_frame = time.ticks_ms()
    frame_start = time.ticks_ms()
    delta = time.ticks_ms() - frame_start
    facing_right = True
    x_velocity = 0
    y_velocity = 0
    x_pos = init_x_pos
    y_pos = init_y_pos
    camera_x = x_pos
    camera_y = y_pos
    camera_velocity = [0,0]
    cat_state_anim = cat_walking
    jumping = False
    global MAX_X_VEL
    global MAX_Y_VEL
    global GRAVITY
    global ACCELERATION
    global CAM_ACCELERATION
    global CAM_DECELERATION
    global MAX_CAM_VELOCITY
    global TILE_HEIGHT
    global TILE_LENGTH
    global ANIMATION_MS_DELAY
    DRAW_DIST_X = thumby.display.width * .8  # Draws x num of pixels both directions from camera_x
    DRAW_DIST_Y = thumby.display.height * .8 # Draws y num of pixels both directions from camera_y
    X_DRAW_RANGE = int(DRAW_DIST_X / TILE_LENGTH)
    Y_DRAW_RANGE = int(DRAW_DIST_Y / TILE_HEIGHT)

    MAX_X_CORD = len(tile_map[0]) - 1
    MAX_Y_CORD = len(tile_map) - 1
    MAX_X_POS = MAX_X_CORD * TILE_LENGTH
    MAX_Y_POS = MAX_Y_CORD * TILE_HEIGHT
    SCREEN_WIDTH = thumby.display.width
    SCREEN_HEIGHT = thumby.display.height
    HALF_SCREEN_WIDTH_INT = SCREEN_WIDTH // 2
    is_falling = False
    debug = False

    while(1):
        frame_start = time.ticks_ms()

        thumby.display.fill(1)

        # Debug stuff
        if debug:
            print(f'Player-  x:{x_pos} y:{y_pos}')
            print(f'Camera-  x:{camera_x} y:{camera_y}')
        thumby.display.drawText(f'x{int(camera_x)} y{int(camera_y)}', 0, 0, 0)
        thumby.display.drawText(f'X{int(x_pos)} Y{int(y_pos)}', 0, thumby.display.height - 8, 0)


        #####################
        #
        #  TILEMAP
        #

        draw_x_cords = []
        draw_y_cords = []

        base_x_cord = int(camera_x / TILE_LENGTH)
        base_y_cord = len(tile_map) - int(camera_y / TILE_HEIGHT) - 1

        # Create bounding box of tilemap
        # x direction
        for x in range(X_DRAW_RANGE * 2 + 1):
            x_cord_to_draw = base_x_cord + x - X_DRAW_RANGE
            if x_cord_to_draw >= 0 and x_cord_to_draw <= MAX_X_CORD:
                draw_x_cords.append(x_cord_to_draw)

        # y direction
        for y in range(Y_DRAW_RANGE * 2 + 1):
            y_cord_to_draw = base_y_cord + y - Y_DRAW_RANGE
            if y_cord_to_draw >= 0 and y_cord_to_draw <= MAX_Y_CORD:
                draw_y_cords.append(y_cord_to_draw)

        if debug:
            print('Tile_MAP:')
            print(f'x:{base_x_cord}, y:{base_y_cord}')
            print(f'drawing x:{draw_x_cords}  y:{draw_y_cords}')
            debug = False

        # Draw tilemap
        for y in draw_y_cords:
            for x in draw_x_cords:
                # draw line from tile map
                tile_num = tile_map[y][x]
                if tile_num == 1:
                    # TODO idk what the -12 is doing...
                    y2 = y - 12
                    thumby.display.drawLine(TILE_LENGTH * x - int(camera_x) + HALF_SCREEN_WIDTH_INT, TILE_HEIGHT * y2 + int(camera_y) + SCREEN_HEIGHT + 6, TILE_LENGTH * (x + 1) - int(camera_x) + HALF_SCREEN_WIDTH_INT, TILE_HEIGHT * y2 + int(camera_y) + SCREEN_HEIGHT + 6, 0)
                    # Under-ledge decor:
                    # Left side
                    thumby.display.drawLine(TILE_LENGTH * x - int(camera_x) + HALF_SCREEN_WIDTH_INT, TILE_HEIGHT * y2 + int(camera_y) + SCREEN_HEIGHT + 8, TILE_LENGTH * x - int(camera_x) + HALF_SCREEN_WIDTH_INT + 2, TILE_HEIGHT * y2 + int(camera_y) + SCREEN_HEIGHT + 8, 0)
                    # Right side
                    thumby.display.drawLine(TILE_LENGTH * (x + 1) - int(camera_x) + HALF_SCREEN_WIDTH_INT - 2, TILE_HEIGHT * y2 + int(camera_y) + SCREEN_HEIGHT + 8, TILE_LENGTH * (x + 1) - int(camera_x) + HALF_SCREEN_WIDTH_INT, TILE_HEIGHT * y2 + int(camera_y) + SCREEN_HEIGHT + 8, 0)


        ##################
        #
        #  PLAYER INPUT
        #

        if thumby.buttonR.pressed() and x_velocity >= 0:
            # TODO Add falling animation for movement
            if not jumping:
                cat_state_anim = cat_walking
            if math.fabs(x_velocity) < MAX_X_VEL:
                x_velocity += ACCELERATION * delta / 10
            facing_right = True
        elif thumby.buttonL.pressed() and x_velocity <= 0:
            if not jumping:
                cat_state_anim = cat_walking
            if math.fabs(x_velocity) < MAX_X_VEL:
                x_velocity -= ACCELERATION * delta / 10
            facing_right = False
        else:
            if math.fabs(x_velocity) <= 2 and not jumping:
                cat_state_anim = cat_standing
            global DECELERATION
            if x_velocity > 0:
                x_velocity -= DECELERATION * delta / 10
                if x_velocity <= 0:
                    x_velocity = 0
                    if not jumping:
                        cat_state_anim = cat_standing

            elif x_velocity < 0:
                x_velocity += DECELERATION * delta / 10
                if x_velocity >= 0:
                    x_velocity = 0
                    if not jumping:
                        cat_state_anim = cat_standing

        if thumby.buttonA.justPressed() and not jumping and y_velocity == 0:
            global JUMP_VELOCITY
            y_velocity = JUMP_VELOCITY
            cat_state_anim = cat_jumping
            jumping = True
            frame_index = 0

        if thumby.buttonB.justPressed() and not debug:
            debug = True

        ##################
        #
        #  PLAYER ANIMATION
        #

        # Flip to next sprite frame after 100 ms
        if time.ticks_ms() > last_anim_frame + ANIMATION_MS_DELAY:
            last_anim_frame = time.ticks_ms()
            frame_index += 1

        # Reset sprite animation
        if frame_index >= len(cat_state_anim):
            frame_index = 0
            # If jumping, animation completed jump
            if jumping:
                jumping = False

        ##################
        #
        #  PLAYER POSITION
        #

        # Calculate cat pos
        x_pos = x_pos + x_velocity * delta / 750
        if x_pos < 0:
            x_pos = 0
        if x_pos > MAX_X_POS:
            x_pos = MAX_X_POS
        y_pos = max(y_pos + y_velocity * delta / 750, -20)

        cat = thumby.Sprite(12, 10, cat_state_anim[frame_index], int(x_pos - camera_x - 6 + int(SCREEN_WIDTH / 2)), int(-y_pos + camera_y - 5 + int(SCREEN_HEIGHT / 2)), 1, mirrorX=0 if facing_right else 1)

        #####################
        #
        #  CALCULATE GRAVITY
        #

        if y_velocity <= 0:
            cat_x_cord_left = int((x_pos + 3) / TILE_LENGTH)
            cat_x_cord_right = int((x_pos - 5) / TILE_LENGTH)
            cat_y_cord = len(tile_map) - int(y_pos / TILE_HEIGHT) - 1
            on_platform = False
            # If is floor tile and next frame's y_vel would pass cat through tile height factor = collision
            if tile_map[cat_y_cord][cat_x_cord_left] == 1 or tile_map[cat_y_cord][cat_x_cord_right] == 1:
                if y_velocity == 0 or y_pos % TILE_HEIGHT < (y_pos + y_velocity * delta / 750) % TILE_HEIGHT:
                    y_velocity = 0
                    is_falling = False
                    on_platform = True
                    if y_pos % TILE_HEIGHT != 0:
                        y_pos = y_pos - (y_pos % TILE_HEIGHT)
            if not on_platform:
                # Calculate falling
                y_velocity = max(y_velocity - GRAVITY * delta / 10, -MAX_Y_VEL)
                is_falling = True
        else:
            # Calculate falling
            y_velocity = max(y_velocity - GRAVITY * delta / 10, -MAX_Y_VEL)
            is_falling = True

        #####################
        #
        #  CAMERA MOVEMENT
        #

        # calculate X camera pos
        if x_pos - camera_x > 18:
            # Right bound
            # camera_x -= 1
            camera_velocity[0] = min(camera_velocity[0] + CAM_ACCELERATION * delta / 10, MAX_CAM_VELOCITY)
        elif x_pos - camera_x < -18:
            # Left bound
            # camera_x += 1
            camera_velocity[0] = max(camera_velocity[0] - CAM_ACCELERATION * delta / 10, -MAX_CAM_VELOCITY)
        else:
            # Deccelerating
            if math.fabs(camera_velocity[0]) <= 2:
                camera_velocity[0] = 0
            else:
                if camera_velocity[0] > 0:
                    camera_velocity[0] = camera_velocity[0] - CAM_DECELERATION * delta / 10
                else:
                    camera_velocity[0] = camera_velocity[0] + CAM_DECELERATION * delta / 10

        # Camera Y Top
        if y_pos - camera_y > -3:
            if camera_y < MAX_Y_POS:
                camera_velocity[1] = min(camera_velocity[1] + CAM_ACCELERATION * delta / 5, MAX_CAM_VELOCITY * 1.5)
        # Camera Y Bottom
        elif y_pos - camera_y < -12:
            if camera_y > 3:
                camera_velocity[1] = max(camera_velocity[1] - CAM_ACCELERATION * delta / 5, -MAX_CAM_VELOCITY * 1.5)
        else:
            if math.fabs(camera_velocity[1]) <= 2:
                camera_velocity[1] = 0
            else:
                if camera_velocity[1] > 0:
                    camera_velocity[1] = camera_velocity[1] - CAM_DECELERATION * delta / 5
                else:
                    camera_velocity[1] = camera_velocity[1] + CAM_DECELERATION * delta / 5

        camera_x = camera_x + camera_velocity[0] * delta / 750
        camera_y = camera_y + camera_velocity[1] * delta / 750

        #####################
        #
        #  UPDATE SCREEN
        #

        thumby.display.drawSprite(cat)
        thumby.display.update()

        # Update delta with frame time
        delta = time.ticks_ms() - frame_start

    thumby.display.update()


def main():
    # Set the FPS (without this call, the default fps is 30)
    lvl1_tile_map = [[1,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1],
                    [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                    [0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                    [0,1,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                    [1,1,1,1,1,1,1,0,0,0,1,1,0,0,0,0,1,1,1,1,0,0,0,0,1,1]]

    thumby.display.setFPS(60)
    global state
    while(1):
        if state == State.MATCH:
            run_level(24, 24, lvl1_tile_map)
        else:
            print('state undefined!')
            print(state)
            return
        thumby.display.update()

main()
