import time
import thumby
import math
import random


class State:
    WELCOME = 1
    MENU = 2
    EXIT = 3
    TUTORIAL = 4
    GAME = 5
    ABOUT = 6
    END = 7
    

state = State.WELCOME
prev_state = State.WELCOME
hearts = 3


def set_small_font():
    thumby.display.setFont("/lib/font5x7.bin", 5, 7, 1)

def set_large_font():
    thumby.display.setFont("/lib/font8x8.bin", 8, 8, 1)

def update_state(new_state):
    global state
    global prev_state
    prev_state = state
    state = new_state
    print(f'State updated: {prev_state}->{state}')

def check_for_menu_nav():
    if (thumby.buttonU.pressed() and thumby.buttonD.pressed()):
        update_state(State.MENU)
        
def format_long_text(text, max_len):
    print('Formatting long text...')
    lines = []
    while len(text) != 0:
        if len(text) <= max_len:
            part = text
            text = ''
        else:
            part = text[0:max_len + 1]
            text = text[max_len + 1:]
            while part[-1] != ' ':
                text = part[-1] + text
                part = part[:-1]
                if len(part) == 1:
                    text = part + text
                    part = text[0:max_len + 1]
                    text = text[max_len + 1:]
                    break
    
        # remove trailing space
        if text != '':
            part = part[:-1]
        lines.append(part)
    return lines


def draw_hearts(x_init, y):
    empty_heart = bytearray([14,49,65,66,132,66,69,49,14])
    full_heart = bytearray([14,63,127,126,252,126,115,63,14])
    global hearts
    for i in range(3):
        x = x_init + 10 * i
        heart = None
        if i >= hearts:
            # draw empty heart
            heart = thumby.Sprite(9, 8, empty_heart)
        else:
            # draw full heart 
            heart = thumby.Sprite(9, 8, full_heart)
        heart.x = x
        heart.y = y
        thumby.display.drawSprite(heart)


def run_welcome():
    set_large_font()
    thumby.display.fill(0) # Fill canvas to black
    thumby.display.drawText('Welcome', 0, 0, 1)
    thumby.display.drawText('to', 18, 12, 1)
    thumby.display.drawText('Title', 6, 24, 1)
    thumby.display.update()
    time.sleep(2)
    thumby.display.fill(0) # Fill canvas to black
    thumby.display.drawText('For Menu', 0, 0, 1)
    thumby.display.drawText('press', 16, 12, 1)
    thumby.display.drawText('+', 32, 28, 1)
    # BITMAP: width: 8, height: 8
    chevron_down_bitmap = bytearray([0,12,24,48,48,24,12,0])
    # BITMAP: width: 8, height: 8
    chevron_up_bitmap = bytearray([0,48,24,12,12,24,48,0])
    chev_down_sprite = thumby.Sprite(8, 8, chevron_down_bitmap, 0)
    chev_up_sprite = thumby.Sprite(8, 8, chevron_up_bitmap, 0)
    chev_down_sprite.x = 14
    chev_down_sprite.y = 28
    chev_up_sprite.x = 50
    chev_up_sprite.y = 28
    thumby.display.drawSprite(chev_down_sprite)
    thumby.display.drawSprite(chev_up_sprite)
    thumby.display.update()
    global state
    start = time.ticks_ms()
    while(state == State.WELCOME):
        check_for_menu_nav()
        if time.ticks_ms() > start + 2000:
            update_state(State.GAME)

def run_menu():
    MIN_MENU_PAGE = 0
    MAX_MENU_PAGE = 1
    menu_page = MIN_MENU_PAGE
    thumby.display.fill(0)
    thumby.display.drawText("Menu", 16, (int(thumby.display.height / 2) - 4), 1)
    thumby.display.update()
    if thumby.buttonA.justPressed() or thumby.buttonB.justPressed():
        pass
    time.sleep(1.5)
    if thumby.buttonA.justPressed() or thumby.buttonB.justPressed():
        pass

    global state
    while (state == State.MENU):
        # BITMAP: width: 8, height: 8
        chevron_down_bitmap = bytearray([0,12,24,48,48,24,12,0])
        # BITMAP: width: 8, height: 8
        chevron_up_bitmap = bytearray([0,48,24,12,12,24,48,0])
        chev_down_sprite = thumby.Sprite(8, 8, chevron_down_bitmap, 0)
        chev_up_sprite = thumby.Sprite(8, 8, chevron_up_bitmap, 0)
        t0 = time.ticks_ms()   # Get time (ms)
        bobRate = 150 # Set arbitrary bob rate (higher is slower)
        bobRange = 2  # How many pixels to move the sprite up/down (-5px ~ 5px)
        # Calculate number of pixels to offset sprite for bob animation
        bobOffset = math.sin(t0 / bobRate) * bobRange
    
        thumby.display.fill(0)
        
        if menu_page == 0:
            thumby.display.drawText("A: Exit", 0, 0, 1)
            thumby.display.drawText("B: Back", 0, 12, 1)
        elif menu_page == 1:
            thumby.display.drawText("A: HowTo", 0, 0, 1)
            thumby.display.drawText("B: Volum", 0, 12, 1)
        # elif menu_page == 2:
        #     thumby.display.drawText("A: About", 0, 0, 1)

    
        # Chevron down
        if menu_page != MAX_MENU_PAGE:
            chev_down_sprite.x = int(thumby.display.width - (thumby.display.width/4))
            chev_down_sprite.y = int(round(thumby.display.height - (thumby.display.height/4) + bobOffset))
            if thumby.buttonD.justPressed():
                time.sleep(0.1)
                menu_page += 1
            thumby.display.drawSprite(chev_down_sprite)

        # Chevron up
        if menu_page != MIN_MENU_PAGE:
            chev_up_sprite.x = int(thumby.display.width - (thumby.display.width/4) - 8)
            chev_up_sprite.y = int(round(thumby.display.height - (thumby.display.height/4) - bobOffset))
            if thumby.buttonU.justPressed():
                time.sleep(0.1)
                menu_page -= 1
            thumby.display.drawSprite(chev_up_sprite)

        # Menu page number
        thumby.display.drawText(f'{menu_page}/{MAX_MENU_PAGE}', 0, height - 8, 1)

        # Add down arrow for more options
        if(thumby.buttonA.justPressed() and menu_page == 0):
            thumby.display.fill(0)
            thumby.display.drawText("Exiting.", 0, 0, 1)
            thumby.display.update()
            for i in range(6):
                thumby.display.drawText('.', i * 8 + 8, 14, 1)
                thumby.display.update()
                time.sleep(0.1)
            time.sleep(0.1)
            update_state(State.EXIT)
        if(thumby.buttonB.justPressed() and menu_page == 0):
            global prev_state
            update_state(prev_state)
        if thumby.buttonA.justPressed() and menu_page == 1:
            update_state(State.TUTORIAL)
        if thumby.buttonA.justPressed and menu_page == 2:
            update_state(State.ABOUT)
        thumby.display.update()


def run_tutorial():
    y_offset = 0
    global state
    width = thumby.display.width
    height = thumby.display.height
    tutorial_text = 'In this game you will do some awesome thing and it will be cool for you'
    tutorial_lines = format_long_text(tutorial_text, 12)
    def draw_tutorial_text(text, y):
        if y < height - 15 + (y_offset * -1):
            thumby.display.drawText(text, 0, y + y_offset, 1)
        
    while state == State.TUTORIAL:
        check_for_menu_nav()
        
        thumby.display.fill(0)
        thumby.display.drawText('Tutorial', 0, 0 + y_offset, 1)
        thumby.display.drawLine(0, 10 + y_offset, width, 10 + y_offset, 1)
        set_small_font()
        y = 14
        for line in tutorial_lines:
            draw_tutorial_text(line, y)
            y += 8
        set_large_font()
        # Bottom footer
        thumby.display.drawLine(0, height - 10, width, height - 10, 1)
        thumby.display.drawText('Done:  A', 0, height - 8, 1)
        
        if thumby.buttonD.pressed():
            if y_offset > -50:
                # print(y_offset)
                y_offset -= 2
        if thumby.buttonU.pressed():
            if y_offset < 0:
                y_offset += 2
                
        if thumby.buttonA.justPressed():
            update_state(State.GAME)
        thumby.display.update()

def ask_rating_question(question_num, question):
    thumby.display.fill(0)
    thumby.display.drawText('QUESTION', 0, 5, 1)
    thumby.display.drawText(f'{question_num}', 30, 20, 1)
    thumby.display.update()
    time.sleep(1.5)
    lines = format_long_text(question, 8)
    set_large_font()
    y_offset = 0
    max_y_offset = (len(lines) - 2) * 10 + 5 if len(lines) >= 3 else 10
    height = thumby.display.height
    width = thumby.display.width
    show_question = True
    star_empty = bytearray([64,160,32,32,32,24,6,1,65,198,24,32,32,32,160,64,
                    0,0,97,153,134,64,64,32,32,64,64,134,153,97,0,0])
    # BITMAP: width: 16, height: 16
    star_full = bytearray([64,224,224,224,224,248,254,255,223,222,56,224,224,224,224,64,
                    0,0,97,249,255,127,127,63,63,127,127,255,249,97,0,0])
    stars = [1,0,0]

    global state
    while state == State.GAME:
        check_for_menu_nav()
        
        thumby.display.fill(0)
        if show_question:
            thumby.display.drawText('Question', 0, 0 + y_offset, 1)
            thumby.display.drawLine(0, 10 + y_offset, width, 10 + y_offset, 1)
            
            def draw_question_text(text, y):
                if y < height - 15 + (y_offset * -1):
                    thumby.display.drawText(text, 0, y + y_offset, 1)
            y = 12
            for line in lines:
                draw_question_text(line, y)
                y += 10
                
            if thumby.buttonD.pressed():
                if y_offset > -max_y_offset:
                    # print(y_offset)
                    y_offset -= 2
            if thumby.buttonU.pressed():
                if y_offset < 0:
                    y_offset += 2
            if thumby.buttonA.justPressed():
                pass
            if thumby.buttonB.justPressed():
                show_question = False
    
            thumby.display.drawLine(0, height - 10, width, height - 10, 1)
            thumby.display.drawText('Done:  B', 0, height - 8, 1)
        else:
            # Stars input
            t0 = time.ticks_ms()
            bobRate = 300
            bobRange = 2

            if thumby.buttonA.justPressed():
                return stars
            if thumby.buttonB.justPressed():
                pass

            draw_hearts(thumby.display.width - 30, 0)
            
            if 0 in stars:
                if thumby.buttonR.justPressed():
                    index = 0
                    for i in range(2):
                        if stars[i] == 0:
                            break
                        else:
                            index += 1
                    stars[index] = 1
            if 1 in stars:
                if thumby.buttonL.justPressed():
                    index = 2
                    for i in range(2):
                        if stars[2 - i] == 1:
                            break
                        else:
                            index -= 1
                    stars[index] = 0
    
            for index, star in enumerate(stars):
                star_sprite = None
                if star == 1:
                    star_sprite = thumby.Sprite(16, 16, star_full, 0)
                else:
                    star_sprite = thumby.Sprite(16, 16, star_empty, 0)
                star_sprite.x = int(20 * index + 8)
                bobOffset = math.sin(t0 / bobRate - 1.5 * index) * bobRange
                y = int(thumby.display.height / 2 - bobOffset - 8) if bobOffset > 0 else int(thumby.display.height / 2 - 8)
                star_sprite.y = y
                thumby.display.drawSprite(star_sprite)
        
            thumby.display.drawText('OK:  A', 8, thumby.display.height - 8, 1)

        thumby.display.update()


def ask_ranking_question():
    thumby.display.fill(0)
    set_small_font()
    thumby.display.drawText('Rank the', 14, 10, 1)
    thumby.display.drawText('following:', 7, 20, 1)
    thumby.display.update()
    time.sleep(2)
    # BITMAP: width: 4, height: 4
    moving_indicator = bytearray([9,15,6,6])
    moving_indicator_sprite = thumby.Sprite(4, 4, moving_indicator)
    cursor_index = 0
    y1 = 10
    y2 = 21
    y3 = 32
    scrolling_x = 0
    cursor_coordinates = [y1, y2, y3]
    options = ['I eat bugs some', 'I eat so much lice its gross if you think about it too much im sick now', 'I don\'t eat at all']
    option_ints = [1, 2, 3]
    moving = False
    last_moved = time.ticks_ms()
    time_since_text_scrolled = None
    show_confirmation = False
    while(1):
        check_for_menu_nav()
        thumby.display.fill(0)
        if show_confirmation:
            if thumby.buttonB.justPressed():
                pass
            thumby.display.drawFilledRectangle(4, 2, thumby.display.width - 4, thumby.display.height - 2, 0)
            thumby.display.drawRectangle(4, 2, thumby.display.width - 4, thumby.display.height - 2, 1)
            # draw_hearts(thumby.display.width - 30, 0)

            set_large_font()
            thumby.display.drawText('Order', 16, 4, 1)
            thumby.display.drawText('OK?', 24, 14, 1)
            thumby.display.drawText('A -- B', 12, 30, 1)
            thumby.display.update()
            
            while(show_confirmation):
                if thumby.buttonB.justPressed():
                    show_confirmation = False
                    set_small_font()
                if thumby.buttonA.justPressed():
                    return option_ints

        else:
            draw_hearts(thumby.display.width - 30, 0)
            if moving:
                moving_indicator_sprite.x = 0
                moving_indicator_sprite.y = cursor_coordinates[cursor_index] + 2
                thumby.display.drawSprite(moving_indicator_sprite)
            else:
                # thumby.display.drawRectangle(0, cursor_coordinates[cursor_index] + 2, 4, 4, 1)
                thumby.display.drawText(str(option_ints[0]), 0, y1, 1)
                thumby.display.drawText(str(option_ints[1]), 0, y2, 1)
                thumby.display.drawText(str(option_ints[2]), 0, y3, 1)
            thumby.display.drawFilledRectangle(7 - scrolling_x, cursor_coordinates[cursor_index] - 1, thumby.display.width + scrolling_x, 10, 1)
            thumby.display.drawText(options[0], 8 - scrolling_x if cursor_index == 0 else 8, y1, 0 if cursor_index == 0 else 1)
            thumby.display.drawText(options[1], 8 - scrolling_x if cursor_index == 1 else 8, y2, 0 if cursor_index == 1 else 1)
            thumby.display.drawText(options[2], 8 - scrolling_x if cursor_index == 2 else 8, y3, 0 if cursor_index == 2 else 1)
    
            if thumby.buttonD.justPressed() and cursor_index != 2:
                cursor_index += 1
                if not moving:
                    scrolling_x = 0
                    last_moved = time.ticks_ms()
                else:
                    prev_index = cursor_index - 1
                    prev = options[prev_index]
                    options[prev_index] = options[cursor_index]
                    options[cursor_index] = prev
                    prev_int = option_ints[prev_index]
                    option_ints[prev_index] = option_ints[cursor_index]
                    option_ints[cursor_index] = prev_int
    
    
            if thumby.buttonU.justPressed() and cursor_index != 0:
                cursor_index -= 1
                if not moving:
                    scrolling_x = 0
                    last_moved = time.ticks_ms()
                else:
                    prev_index = cursor_index + 1
                    prev = options[prev_index]
                    options[prev_index] = options[cursor_index]
                    options[cursor_index] = prev
                    prev_int = option_ints[prev_index]
                    option_ints[prev_index] = option_ints[cursor_index]
                    option_ints[cursor_index] = prev_int
    
    
            if thumby.buttonR.justPressed() and not moving:
                moving = True
                scrolling_x = 0
                time_since_text_scrolled = None
    
            
            if thumby.buttonL.justPressed() and moving:
                moving = False
                last_moved = time.ticks_ms()
                
            if time.ticks_ms() > last_moved + 1500 and not moving:
                if len(options[cursor_index]) * 6 - 60 > scrolling_x:
                    scrolling_x += 1
                    time_since_text_scrolled = time.ticks_ms()
                else:
                    if time_since_text_scrolled + 1500 < time.ticks_ms():
                        scrolling_x = 0
                        last_moved = time.ticks_ms()
                        time_since_text_scrolled = None
            
            if thumby.buttonA.justPressed():
                show_confirmation = True

        thumby.display.update()


def show_results(expected, actual):
    global hearts
    # BITMAP: width: 9, height: 8
    empty_heart = bytearray([14,49,65,66,132,66,69,49,14])
    full_heart = bytearray([14,63,127,126,252,126,115,63,14])
    thumby.display.fill(0)
    lost_heart = False
    set_large_font()
    if actual == expected:
        thumby.display.drawText('Correct!', 0, 10, 1)
    else:
        hearts -= 1
        lost_heart = True
        thumby.display.drawText('Wrong :(', 0, 10, 1)
    
    thumby.display.update()
    time.sleep(0.5)
    start = time.ticks_ms()
    j = 0
    while time.ticks_ms() < start + 1500:
        for i in range(3):
            x = 32 + 10 * i
            y = int(thumby.display.height / 2 + 4)
            heart = None
            if i >= hearts:
                # draw empty heart
                
                # draw a full heart every .5 seconds if heart was just lost
                if lost_heart and i == hearts and (time.ticks_ms() - start) % 750 < 250:
                    heart = thumby.Sprite(9, 8, full_heart, 9, 8)
                else:
                    heart = thumby.Sprite(9, 8, empty_heart, 9, 8)

            else:
                # draw full heart 
                heart = thumby.Sprite(9, 8, full_heart, 9, 8)
            heart.x = x
            heart.y = y
            thumby.display.drawSprite(heart)
            thumby.display.update()
    # Draw all correct hearts for final 0.5 seconds
    draw_hearts(32, int(thumby.display.height / 2 + 4))
    thumby.display.update()
    time.sleep(0.5)
    if hearts == 0:
        global state
        state = State.END


def show_victory():
    thumby.display.fill(0)
    thumby.display.drawText('YOU WIN!', 0, 10, 1)
    thumby.display.update()
    time.sleep(1)
    return
    

def run_game():
    rating_questions = [
        'How are your lover\'s kisses?',
        'How well does your lover sleep?',
        'How would you rate your lover\'s cleanli ness?',
        'How would you rate your lover\'s physical fitness?',
        'How much do you like chocolate?',
        'How much do you like your lover\'s family?',
        'How satis fied are you with life?',
        'How much do you want to kiss your lover right now?',
        'How would you rate your relation ship?',
        'How would you rate your timeliness?',
        'How responsible is your lover?',
        'How much does your lover love you?',
        'How would you rate the equality in your relation ship?',
        'How would your lover rate your looks?',
        'How would your lover rate your family?',
        'How much do you like babies?',
        'How much does your lover like your family?'
        ]
    ranking_questions = [
        ['I love chocolate above all else', 'Eating healthy is my jam', 'Something salty plz!'],
        ['Bookworm, that is me', 'I\'m what you call a pro gamer', 'Making something with my hands keeps me busy'],
        ['Fear of heights', 'Fear of spiders', 'Fear of rejection'],
        ['Cake', 'Pie', 'Cookie'],
        ['Beach', 'Mountains', 'City'],
        ['Blue', 'Purple', 'Red'],
        ['Yellow', 'Beige', 'Green'],
        ['Die alone', 'Lose all material things', 'Become paralyzed'],
        ['Live in the moment', 'Savor the memories', 'Plan for new memories'],
        ['8 bit', 'Polygons', 'Ray Tracing'],
        ['Red Meat', 'White Meat', 'Tofu'],
        ['Monday', 'Thursday', 'Saturday'],
        ['New pair of shoes', 'New pair of headphones', 'New pair of tea cups'],
        ['1700\'s', '1800\'s', '1900\'s'],
        ['Bell bottoms', 'Skinnys', 'Boot cuts'],
        ['Lions', 'Tigers', 'Bears'],
        ['Who would win a battle to the death? 1 horse', '3 small monkeys', '500 rats'],
        ['Who would win a battle of wits? Myself', 'My lover', 'Their mom'],
        ['Who would win an eating contest? Myself', 'My lover', 'Their ex'],
        ['Who would win best style? Myself', 'My lover', 'Their dad'],
        ['Peanuts', 'Cashews', 'Almonds'],
        ['Cats', 'Dogs', 'Any other pet'],
        ['Sushi', 'Pasta', 'Tacos'],
        ['Soda', 'Milk', 'Juice'],
        ['Sweats', 'Hoody', 'T-Shirts'],
        ['Beautiful art', 'Art that make you think', 'Incomprehensible art'],
        ['My career and aspirations', 'My hobbies and pastimes', 'My better half'],
        ['I respect you', 'I love you', 'I believe you'],
        ]
    question_num = 1
    num_questions = 6
    
    def ask_rating():
        index = random.randint(0, (len(rating_questions)-1))
        question = rating_questions[index]
        rating_questions = rating_questions[0:index] + rating_questions[index+1:]
        stars = ask_rating_question(question_num, question)
        # get answer from other player
        answer_stars = [1,1,0]
        show_results(answer_stars, stars)
        question_num += 1
    
    global state
    while state == State.GAME and question_num < num_questions:
        # q_type = random.randint(0, 3)
        # if q_type == 0:
        #     ask_rating()
        # else:
        #     ask_rating()
        res = ask_ranking_question()
        show_results([3,2,1], res)
        question_num += 1
    show_victory()


def run_end():
    thumby.display.fill(0)
    thumby.display.drawText('GAME', 16, 5, 1)
    thumby.display.drawText('OVER', 16, 18, 1)
    thumby.display.update()
    time.sleep(2)
    return


def main():
    # Set the FPS (without this call, the default fps is 30)
    thumby.display.setFPS(60)
    # https://thumby.us/API/Text-and-Font/
    while(1):
        # print(f'State: {state}')
        if state == State.WELCOME:
            run_welcome()
        elif state == State.MENU:
            run_menu()
        elif state == State.EXIT:
            thumby.display.fill(0)
            thumby.display.update()
            return 0
        elif state == State.TUTORIAL:
            run_tutorial()
        elif state == State.GAME:
            run_game()
        elif state == State.ABOUT:
            print('TODO')
        elif state == State.END:
            run_end()
        else:
            print('state undefined!')
            print(state)
            return
        thumby.display.update()
    
main()
